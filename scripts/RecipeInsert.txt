-- Create a proc to insert a new recipe into the database.
-- Created 6/5/2013 by Philip Gedgaud

DELIMITER $$

CREATE PROCEDURE RecipeInsert(
	IN recName varchar(100)
	, IN recDesc varchar(500)
	, IN recCreateBy varchar(100)
	, OUT recID int)
BEGIN
	INSERT INTO recipe 
	(name     		
	, description		
	, createBy
	, modifiedBy 
	)
	VALUES
	(recName
	, recDesc  
	, recCreateBy 
	, recCreateBy 
	);
	
	recID = last inserted id;
END$$
DELIMITER ;