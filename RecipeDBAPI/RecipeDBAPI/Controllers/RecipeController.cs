﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using RecipeDBAPI.Models;
using System.Web.Http.ModelBinding;

namespace RecipeDBAPI.Controllers
{
    public class RecipeController : ApiController
    {
        // GET api/values
        public IEnumerable<Recipe> Get()
        {
            List<Recipe> results = new List<Recipe>();

            MongoClient client = new MongoClient();
            MongoServer server = client.GetServer();
            MongoDatabase recipes = server.GetDatabase("recipeDB");
            MongoCollection recColl = recipes.GetCollection("recipe");
            MongoCursor cursor = recColl.FindAllAs<BsonDocument>();
            
            foreach (BsonDocument item in cursor)
            {
                Recipe rec = new Recipe();
                rec.Id = item.GetValue("_id").ToString();
                rec.Category = item.GetElement("Category").Value.ToString();
                rec.Instructions = item.GetElement("Instructions").Value.ToString();
                rec.Name = item.GetElement("Name").Value.ToString();
                rec.Notes = item.GetElement("Notes").Value.ToString();
                rec.RecipeID = item.GetElement("RecipeID").Value.ToInt32();
                results.Add(rec);   
            }

            return results;
        }

        // GET api/values/5
        public Recipe Get(string id)
        {
            Recipe result = new Recipe();
            MongoClient client = new MongoClient();
            MongoServer server = client.GetServer();
            MongoDatabase recipes = server.GetDatabase("recipeDB");
            MongoCollection recColl = recipes.GetCollection("recipe");
            QueryDocument query = new QueryDocument("_id", ObjectId.Parse(id));
            BsonDocument doc = recColl.FindOneAs<BsonDocument>(query);
            result.Ingredients = new List<Ingredient>();
            foreach (BsonDocument item in doc.GetElement("Ingredients").Value.AsBsonArray)
            {
                Ingredient ing = new Ingredient();
                ing.Quanity = item.GetElement("Quanity").Value.ToInt32();
                ing.Measurement = item.GetElement("Measurement").Value.ToString();
                ing.Name = item.GetElement("Name").Value.ToString();
                result.Ingredients.Add(ing);
            }
            result.Id = doc.GetValue("_id").ToString();
            result.Category = doc.GetElement("Category").Value.ToString();
            result.Instructions = doc.GetElement("Instructions").Value.ToString();
            result.Name = doc.GetElement("Name").Value.ToString();
            result.Notes = doc.GetElement("Notes").Value.ToString();
            result.RecipeID = doc.GetElement("RecipeID").Value.ToInt32();
            return result;
        }

        // POST api/values
        [HttpPost]
        public HttpResponseMessage Post([FromBody]Recipe value)
        {
            if (ModelState.IsValid)
            {
                BsonDocument recipe = value.ToBsonDocument();
                recipe.Set("_id", ObjectId.GenerateNewId());
                MongoClient client = new MongoClient();
                MongoServer server = client.GetServer();
                MongoDatabase recipeDB = server.GetDatabase("recipeDB");
                MongoCollection recCollection = recipeDB.GetCollection("recipe");
                recCollection.Insert(recipe);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // PUT api/values/5
        [HttpPut]
        public HttpResponseMessage Put(string id, [FromBody]Recipe value)
        {
            if (ModelState.IsValid)
            {
                Recipe result = new Recipe();
                MongoClient client = new MongoClient();
                MongoServer server = client.GetServer();
                MongoDatabase recipes = server.GetDatabase("recipeDB");
                MongoCollection recColl = recipes.GetCollection("recipe");
                QueryDocument query = new QueryDocument("_id", ObjectId.Parse(id));
                UpdateDocument update = new UpdateDocument(value.ToBsonDocument());
                update.Set("_id", ObjectId.Parse(value.Id));
                recColl.Update(query, update);  
                
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/values/5
        public void Delete(string id)
        {
            MongoClient client = new MongoClient();
            MongoServer server = client.GetServer();
            MongoDatabase recipes = server.GetDatabase("recipeDB");
            MongoCollection recColl = recipes.GetCollection("recipe");
            QueryDocument query = new QueryDocument("_id", ObjectId.Parse(id));
            recColl.Remove(query);
        }
    }
}
