﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecipeDBAPI.Models
{
    public class Measurement
    {
        public int MeasurementId { get; set; }
        //[DataMember(Name = "name")]
        public string Name { get; set; }
        //[DataMember(Name = "abbreviation")]
        public string Abbreviation { get; set; }
    }
}
