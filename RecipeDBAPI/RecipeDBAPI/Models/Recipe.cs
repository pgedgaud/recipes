﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace RecipeDBAPI.Models
{
    //[DataModel]
    public class Recipe
    {
        //[DataMember]
        
        public string Id { 
            get; 
            set;  
        }
        public int RecipeID { get; set; }
        //[DataMember(Name = "name")]
        public string Name { get; set; }
        //[DataMember(Name = "ingredients")]
        public List<Ingredient> Ingredients { get; set; }
        //[DataMember(Name = "instructions")]
        public string Instructions { get; set; }
        //[DataMember(Name = "category")]
        public string Category { get; set; }
        //[DataMember(Name = "notes")]
        public string Notes { get; set; }
    }
}