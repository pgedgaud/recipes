﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecipeDBAPI.Models
{
    //[DataContract]
    public class Ingredient
    {

        //public int IngredientId { get; set; }
        //[DataMember(Name = "quanity")]
        public int Quanity { get; set; }
        //[DataMember(Name = "measurement")]
        public string Measurement { get; set; }
        //[DataMember(Name = "name")]
        public string Name { get; set; }
    }
}
