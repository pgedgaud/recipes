﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(recipeDB.Startup))]
namespace recipeDB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
