﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using recipeDB.Models;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Json;
using System.Text;


namespace recipeDB
{
    public class RecipeController : Controller
    {
        //private RecipeDBContext db = new RecipeDBContext();
        private List<Recipe> recipes = new List<Recipe>(); 

        // GET: Recipe
        public ActionResult Index()
        {
            WebRequest request = WebRequest.Create("http://localhost:49922/api/Recipe");
            WebResponse response = request.GetResponse();
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            Stream dataStream = response.GetResponseStream();
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(List<Recipe>));
            object objResponse = jsonSerializer.ReadObject(dataStream);
            List<Recipe> jsonResponse = objResponse as List<Recipe>;
            response.Close();
            return View(jsonResponse);
        }

        // GET: Recipe/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            WebRequest request = WebRequest.Create("http://localhost:49922/api/Recipe/" + id);
            WebResponse response = request.GetResponse();
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            Stream dataStream = response.GetResponseStream();
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Recipe));
            object objResponse = jsonSerializer.ReadObject(dataStream);
            Recipe jsonResponse = objResponse as Recipe;
            response.Close();

            if (jsonResponse == null)
            {
                return HttpNotFound();
            }

            return View(jsonResponse);
        }

        // GET: Recipe/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Recipe/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RecipeID,Name,Instructions,Category,Notes")] Recipe recipe)
        {
            string jsonString;

            if (ModelState.IsValid)
            {
                recipes.Add(recipe);
                
                MemoryStream stream = new MemoryStream();
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Recipe));
                ser.WriteObject(stream, recipe);
                stream.Position = 0;
                
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    jsonString = reader.ReadToEnd();
                }

                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:49922/api/Recipe");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(jsonString);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string result = streamReader.ReadToEnd();
                }

                return RedirectToAction("Index");
            }

            return View(recipe);
        }

        // GET: Recipe/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            WebRequest request = WebRequest.Create("http://localhost:49922/api/Recipe/" + id);
            WebResponse response = request.GetResponse();
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            Stream dataStream = response.GetResponseStream();
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Recipe));
            object objResponse = jsonSerializer.ReadObject(dataStream);
            Recipe jsonResponse = objResponse as Recipe;
            response.Close();

            if (jsonResponse == null)
            {
                return HttpNotFound();
            }

            return View(jsonResponse);
        }

        // POST: Recipe/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RecipeID,Name,Ingerdients,Instructions,Category,Notes")] Recipe recipe)
        {
            Ingredient ing = new Ingredient();
            ing.Quanity = 1;
            ing.Measurement = "cups";
            ing.Name = "peanut butter";

            Ingredient ing2 = new Ingredient();
            ing2.Quanity = 2;
            ing2.Measurement = "cups";
            ing2.Name = "jelly";
            recipe.Ingredients = new List<Ingredient>();
            recipe.Ingredients.Add(ing);
            recipe.Ingredients.Add(ing2);

            string jsonString;

            if (ModelState.IsValid)
            {
                MemoryStream stream = new MemoryStream();
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Recipe));
                ser.WriteObject(stream, recipe);
                stream.Position = 0;

                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    jsonString = reader.ReadToEnd();
                }

                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:49922/api/Recipe/" + recipe.Id);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "PUT";

                using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(jsonString);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string result = streamReader.ReadToEnd();
                }

                return RedirectToAction("Index");
            }
            
            return View(recipe);
        }


        // GET: Recipe/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Recipe recipe = recipes.Find(rec => rec.RecipeID.Equals(id));
            
            if (recipe == null)
            {
                return HttpNotFound();
            }
            
            return View(recipe);
        }

        // POST: Recipe/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Recipe recipe = recipes.Find(rec => rec.RecipeID.Equals(id));
           
            return RedirectToAction("Index");
        }



        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        public ActionResult RecipeIngredientEntryRow()
        {
            return PartialView("RecipeIngredients", new Ingredient());
        }
    }
}
