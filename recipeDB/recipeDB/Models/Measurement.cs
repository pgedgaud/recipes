﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace recipeDB.Models
{
    [DataContract]
    public class Measurement
    {
        public int MeasurementId { get; set; }
        [DataMember(Name = "Name")]
        public string Name { get; set; }
        [DataMember(Name = "Abbreviation")]
        public string Abbreviation { get; set; }
    }
}
