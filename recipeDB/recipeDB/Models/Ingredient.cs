﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace recipeDB.Models
{
    [DataContract]
    public class Ingredient
    {
        [DataMember(Name = "Quanity")]
        public int Quanity { get; set; }
        [DataMember(Name = "Measurement")]
        public string Measurement { get; set; }
        [DataMember(Name = "Name")]
        public string Name { get; set; }
    }
}
