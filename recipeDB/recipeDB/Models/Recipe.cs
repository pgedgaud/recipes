﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Json;
using System.ServiceModel;

namespace recipeDB.Models
{
    [DataContract]
    public class Recipe
    {
        [DataMember(Name = "Id")]
        public string Id { get; set; }
        [DataMember(Name = " RecipeID")]
        public int RecipeID { get; set; }
        [DataMember(Name = "Name")]
        public string Name { get; set; }
        [DataMember(Name = "Ingredients")]
        public List<Ingredient> Ingredients { get; set; }
        [DataMember(Name = "Instructions")]
        public string Instructions { get; set; }
        [DataMember(Name = "Category")]
        public string Category { get; set; }
        [DataMember(Name = "Notes")]
        public string Notes { get; set; }
    }
}